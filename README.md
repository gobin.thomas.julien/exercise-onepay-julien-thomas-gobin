# Exercise OnePay - Julien Thomas Gobin

## Validation

**- Creation of a transaction with specific values**

Call of the localhost:8080/transaction with POST Method and the body:

    {
        "total":54.80,
        "paymentMethod":"CREDIT_CARD",
        "status":"NEW",
        "command":{
            "id":1,
            "lines":[
                {
                    "quantity":4,
                    "productName":"Cool pair of ski gloves",
                    "price":10.00
                },
                {
                    "quantity":1,
                    "productName":"Nice wool cap",
                    "price":14.80
                }
            ]
        }
    }

**- Update of the transaction to the AUTHORIZED state**

Call of the localhost:8080/transaction with POST Method and the body:

    {
        "status":"AUTHORIZED",
        "command":{
            "id":1
        }
    }

**- Update of the transaction to the CAPTURED state**

Call of the localhost:8080/transaction with POST Method and the body:

    {
        "status":"CAPTURED",
        "command":{
          "id":1
        }
    }

**- Creation of another transaction with specific values**

Call of the localhost:8080/transaction with POST Method and the body:

    {
        "total":208.00,
        "paymentMethod":"PAYPAL",
        "status":"NEW",
        "command":{
            "id":2,
            "lines":[
                {
                    "quantity":1,
                    "productName":"Funky bike",
                    "price":208.00
                }
            ]
        }
    }

**- Recuperation of all the created transaction**

Call of the localhost:8080/transaction with GET Method