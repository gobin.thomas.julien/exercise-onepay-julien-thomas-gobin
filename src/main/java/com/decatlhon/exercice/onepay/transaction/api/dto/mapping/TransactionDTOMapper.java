package com.decatlhon.exercice.onepay.transaction.api.dto.mapping;

import com.decatlhon.exercice.onepay.command.api.dto.mapping.CommandDTOMapper;
import com.decatlhon.exercice.onepay.transaction.api.dto.TransactionDTO;
import com.decatlhon.exercice.onepay.transaction.service.bean.Transaction;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.PaymentMethod;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.TransactionStatus;
import com.decatlhon.exercice.onepay.transaction.service.exception.TransactionDTOBadDataException;
import org.springframework.util.ObjectUtils;

public class TransactionDTOMapper {

    public static Transaction dtoToBean(TransactionDTO dto) throws TransactionDTOBadDataException {
        return Transaction.builder()
                .command(CommandDTOMapper.dtoToBean(dto.getCommand()))
                .paymentMethod(paymentMethod(dto.getPaymentMethod()))
                .status(transactionStatus(dto.getStatus()))
                .total(dto.getTotal())
                .build();
    }

    private static PaymentMethod paymentMethod(String paymentMethodDto) throws TransactionDTOBadDataException {
        try {
            return ObjectUtils.isEmpty(paymentMethodDto) ? null : PaymentMethod.valueOf(paymentMethodDto);
        } catch (IllegalArgumentException ex) {
            throw new TransactionDTOBadDataException("the method payment " + paymentMethodDto + " is not available");
        }
    }

    private static TransactionStatus transactionStatus(String transactionStatusDto) throws TransactionDTOBadDataException {
        try {
            return ObjectUtils.isEmpty(transactionStatusDto) ? null : TransactionStatus.valueOf(transactionStatusDto);
        } catch (IllegalArgumentException ex) {
            throw new TransactionDTOBadDataException("the status " + transactionStatusDto + " does not exist");
        }
    }


    public static TransactionDTO beanToDto(Transaction bean) {
        return TransactionDTO.builder()
                .command(CommandDTOMapper.beanToDTO(bean.getCommand()))
                .paymentMethod(bean.getPaymentMethod().name())
                .status(bean.getStatus().name())
                .total(bean.getTotal())
                .build();
    }
}
