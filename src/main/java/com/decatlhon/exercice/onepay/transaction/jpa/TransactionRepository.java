package com.decatlhon.exercice.onepay.transaction.jpa;

import com.decatlhon.exercice.onepay.transaction.jpa.entities.TransactionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionEntity, Integer> {
}
