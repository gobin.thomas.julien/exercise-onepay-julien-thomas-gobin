package com.decatlhon.exercice.onepay.transaction.service.exception;

public class TransactionException extends Exception {
    public TransactionException(String message) {
        super(message, null);
    }

    public TransactionException(String message, Exception cause) {
        super(message, cause);
    }
}
