package com.decatlhon.exercice.onepay.transaction.service.bean.mapping;

import com.decatlhon.exercice.onepay.command.service.bean.Command;
import com.decatlhon.exercice.onepay.transaction.jpa.entities.TransactionEntity;
import com.decatlhon.exercice.onepay.transaction.service.bean.Transaction;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.PaymentMethod;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.TransactionStatus;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;

public class TransactionBeanMapper {

    public static TransactionEntity beanToEntity(Transaction bean) {
        return TransactionEntity.builder()
                .commandId(bean.getCommand().getId())
                .paymentMethod(ObjectUtils.isEmpty(bean.getPaymentMethod()) ? null : bean.getPaymentMethod().name())
                .status(ObjectUtils.isEmpty(bean.getStatus()) ? null : bean.getStatus().name())
                .total(bean.getTotal())
                .build();
    }

    public static Transaction entityToBean(TransactionEntity entity) {
        return Transaction.builder()
                .command(Command.builder().id(entity.getCommandId()).lines(new ArrayList<>()).build())
                .paymentMethod(paymentMethod(entity.getPaymentMethod()))
                .status(transactionStatus(entity.getStatus()))
                .total(entity.getTotal())
                .build();
    }

    private static PaymentMethod paymentMethod(String paymentMethodBDD) {
        return PaymentMethod.valueOf(paymentMethodBDD);
    }

    private static TransactionStatus transactionStatus(String transactionStatusBDD) {
        return TransactionStatus.valueOf(transactionStatusBDD);
    }
}
