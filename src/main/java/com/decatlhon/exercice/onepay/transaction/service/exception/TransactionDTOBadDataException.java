package com.decatlhon.exercice.onepay.transaction.service.exception;

public class TransactionDTOBadDataException extends TransactionException {
    public TransactionDTOBadDataException(String message) {
        super(message, null);
    }

    public TransactionDTOBadDataException(String message, Exception cause) {
        super(message, cause);
    }
}
