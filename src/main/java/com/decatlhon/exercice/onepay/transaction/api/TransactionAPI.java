package com.decatlhon.exercice.onepay.transaction.api;

import com.decatlhon.exercice.onepay.transaction.api.dto.TransactionDTO;
import com.decatlhon.exercice.onepay.transaction.api.dto.mapping.TransactionDTOMapper;
import com.decatlhon.exercice.onepay.transaction.service.TransactionService;
import com.decatlhon.exercice.onepay.transaction.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("transaction")
public class TransactionAPI {

    @Autowired
    private TransactionService service;

    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<String> handleTransactionException(TransactionException ex) {

        // [Commentary only for the exercise] This could be managed externally in an AdviceController, in most team I
        // was in when there was not a lot of exception handling this was the chosen option.

        if (ex instanceof TransactionConfigurationException) {
            return ResponseEntity.internalServerError().body(ex.getMessage());
        }

        if (ex instanceof TransactionDTOBadDataException || ex instanceof TransactionLifecycleNotRespectedException || ex instanceof TransactionRestrictedFieldNotRespectedException) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }

        return ResponseEntity.internalServerError().body(ex.getMessage());
    }

    @PostMapping
    public ResponseEntity<TransactionDTO> postTransaction(@RequestBody TransactionDTO dto) throws TransactionException {
        TransactionDTO postedTransaction = TransactionDTOMapper.beanToDto(service.postTransaction(TransactionDTOMapper.dtoToBean(dto)));

        return ResponseEntity.ok().body(postedTransaction);
    }

    @GetMapping
    public ResponseEntity<List<TransactionDTO>> findAllTransaction() {
        List<TransactionDTO> foundTransactionList = service.findAllTransaction().stream().map(TransactionDTOMapper::beanToDto).collect(Collectors.toList());

        return ResponseEntity.ok().body(foundTransactionList);
    }
}
