package com.decatlhon.exercice.onepay.transaction.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "transaction")
public class TransactionProperties {
    private List<String> statusLifecycle;
}
