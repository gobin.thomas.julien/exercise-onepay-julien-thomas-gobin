package com.decatlhon.exercice.onepay.transaction.service;

import com.decatlhon.exercice.onepay.command.service.CommandService;
import com.decatlhon.exercice.onepay.transaction.jpa.TransactionRepository;
import com.decatlhon.exercice.onepay.transaction.jpa.entities.TransactionEntity;
import com.decatlhon.exercice.onepay.transaction.service.bean.Transaction;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.PaymentMethod;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.TransactionStatus;
import com.decatlhon.exercice.onepay.transaction.service.bean.mapping.TransactionBeanMapper;
import com.decatlhon.exercice.onepay.transaction.service.config.TransactionProperties;
import com.decatlhon.exercice.onepay.transaction.service.exception.TransactionConfigurationException;
import com.decatlhon.exercice.onepay.transaction.service.exception.TransactionException;
import com.decatlhon.exercice.onepay.transaction.service.exception.TransactionLifecycleNotRespectedException;
import com.decatlhon.exercice.onepay.transaction.service.exception.TransactionRestrictedFieldNotRespectedException;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Log
@Service
public class TransactionService {

    @Autowired
    private TransactionProperties properties;

    @Autowired
    private TransactionRepository repository;

    @Autowired
    private CommandService commandService;

    private TransactionStatus newStatus;

    public Transaction postTransaction(Transaction bean) throws TransactionException {
        fillEmptyFields(bean);

        assertNewBeanRespectLifeCycle(bean);
        assertNewBeanRespectUpdateRestriction(bean);

        synchronizeBeanWithBDD(bean);
        synchronizeCommand(bean);

        return TransactionBeanMapper.entityToBean(repository.save(TransactionBeanMapper.beanToEntity(bean)));
    }

    private void synchronizeCommand(Transaction bean) {
        if (TransactionStatus.NEW.equals(bean.getStatus())) {
            // [Commentary only for the exercise] Depending on the context in fact, and the command may be handled by
            // another service. But it feels coherent to at least update the command when the first transaction is saved.
            commandService.postCommand(bean.getCommand());
        }
    }

    private void fillEmptyFields(Transaction bean) throws TransactionException {
        if (ObjectUtils.isEmpty(bean.getStatus())) {
            bean.setStatus(newStatus());
        }
    }

    private void synchronizeBeanWithBDD(Transaction bean) {
        Optional<TransactionEntity> fromBDD = repository.findById(bean.getCommand().getId());
        fromBDD.ifPresent(entity -> {
                bean.setTotal(entity.getTotal());
                bean.setPaymentMethod(PaymentMethod.valueOf(entity.getPaymentMethod()));
        });
    }

    private void assertNewBeanRespectLifeCycle(Transaction bean) throws TransactionException {
        Optional<TransactionEntity> fromBDD = repository.findById(bean.getCommand().getId());
        if (fromBDD.isPresent()) {
            int bddStatusOffset = properties.getStatusLifecycle().indexOf(fromBDD.get().getStatus());
            int beanStatusOffset = properties.getStatusLifecycle().indexOf(bean.getStatus().name());

            if (beanStatusOffset != bddStatusOffset && beanStatusOffset != bddStatusOffset + 1) {
               throw new TransactionLifecycleNotRespectedException("The transaction cannot be updated to " + bean.getStatus().name() + " from " + fromBDD.get().getStatus());
            }
        }
        else {
            if (!bean.getStatus().equals(newStatus())) {
                throw new TransactionLifecycleNotRespectedException("New transaction must be set to the status " + newStatus().name() + " and not " + bean.getStatus());
            }
        }
    }

    private void assertNewBeanRespectUpdateRestriction(Transaction bean) throws TransactionRestrictedFieldNotRespectedException {

        // [Commentary only for the exercise] This was not asked for the exercise, BUT it feels just to necessary to have some sort of field update
        // restriction for after a transaction is Authorized.
        if (TransactionStatus.NEW.equals(bean.getStatus())) {
            return;
        }

        Optional<TransactionEntity> fromBDD = repository.findById(bean.getCommand().getId());
        if (fromBDD.isPresent()) {
            TransactionEntity entity = fromBDD.get();
            if (bean.getTotal() != null && !bean.getTotal().equals(entity.getTotal())) {
                throw new TransactionRestrictedFieldNotRespectedException("You cannot change the total of a transaction once it's past AUTHORIZED");
            }
            if (bean.getPaymentMethod() != null && !bean.getPaymentMethod().name().equals(entity.getPaymentMethod())) {
                throw new TransactionRestrictedFieldNotRespectedException("You cannot change the payment method of a transaction once it's past AUTHORIZED");
            }
        }
    }

    public List<Transaction> findAllTransaction() {
        return StreamSupport.stream(repository.findAll().spliterator(), false).map(TransactionBeanMapper::entityToBean).collect(Collectors.toList());
    }

    private TransactionStatus newStatus() throws TransactionException {
        if (newStatus == null) {
            try {
                newStatus = TransactionStatus.valueOf(properties.getStatusLifecycle().get(0));
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                throw new TransactionConfigurationException("There is no status specified in the lifecycle of transaction", ex);
            } catch (IllegalArgumentException ex) {
                throw new TransactionConfigurationException("The specified first status for lifecycle doesn't exist", ex);
            }
        }

        return newStatus;
    }
}
