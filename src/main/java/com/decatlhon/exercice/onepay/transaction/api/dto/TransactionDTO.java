package com.decatlhon.exercice.onepay.transaction.api.dto;

import com.decatlhon.exercice.onepay.command.api.dto.CommandDTO;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TransactionDTO {
    // [Commentary only for the exercise] In the real world we probably should use another format that include the Currency type.
    private BigDecimal total;

    private String paymentMethod;
    private String status;

    private CommandDTO command;
}
