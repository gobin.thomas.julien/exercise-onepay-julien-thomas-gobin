package com.decatlhon.exercice.onepay.transaction.service.exception;

public class TransactionConfigurationException extends TransactionException {
    public TransactionConfigurationException(String message) {
        super(message, null);
    }

    public TransactionConfigurationException(String message, Exception cause) {
        super(message, cause);
    }
}
