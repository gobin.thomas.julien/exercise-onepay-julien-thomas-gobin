package com.decatlhon.exercice.onepay.transaction.service.bean.data;

public enum PaymentMethod {
    CREDIT_CARD, GIFT_CARD, PAYPAL
}
