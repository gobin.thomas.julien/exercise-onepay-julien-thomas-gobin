package com.decatlhon.exercice.onepay.transaction.service.exception;

public class TransactionRestrictedFieldNotRespectedException extends TransactionException {
    public TransactionRestrictedFieldNotRespectedException(String message) {
        super(message, null);
    }

    public TransactionRestrictedFieldNotRespectedException(String message, Exception cause) {
        super(message, cause);
    }
}
