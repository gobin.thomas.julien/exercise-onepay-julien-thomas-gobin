package com.decatlhon.exercice.onepay.transaction.service.bean;

import com.decatlhon.exercice.onepay.command.service.bean.Command;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.PaymentMethod;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.TransactionStatus;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class Transaction {
    // [Commentary only for the exercise] In the real world we probably should use another format that include the Currency type.
    private BigDecimal total;

    private PaymentMethod paymentMethod;
    private TransactionStatus status;

    private Command command;
}
