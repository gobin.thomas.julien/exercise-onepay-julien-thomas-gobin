package com.decatlhon.exercice.onepay.transaction.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "transaction")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionEntity implements Serializable {

    /* [Commentary only for the exercise] In the real world we should probably create a composite Id because command can
     * have multiple transaction. But it feels out of the scope of an exercise. */
    @Id
    @Column(name = "id")
    private Integer commandId;

    // [Commentary only for the exercise] In the real world we probably should use another format that include the Currency type.
    @Column(name = "total", nullable = false)
    private BigDecimal total;

    @Column(name = "paymentMethod", nullable = false)
    private String paymentMethod;

    @Column(name = "status", nullable = false)
    private String status;

    public Integer getCommandId() {
        return commandId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getStatus() {
        return status;
    }
}
