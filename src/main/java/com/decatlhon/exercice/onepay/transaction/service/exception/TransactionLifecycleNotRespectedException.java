package com.decatlhon.exercice.onepay.transaction.service.exception;

public class TransactionLifecycleNotRespectedException extends TransactionException {
    public TransactionLifecycleNotRespectedException(String message) {
        super(message, null);
    }

    public TransactionLifecycleNotRespectedException(String message, Exception cause) {
        super(message, cause);
    }
}
