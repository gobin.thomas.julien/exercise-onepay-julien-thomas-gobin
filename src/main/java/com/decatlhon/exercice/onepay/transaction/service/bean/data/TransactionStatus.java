package com.decatlhon.exercice.onepay.transaction.service.bean.data;

public enum TransactionStatus {
    NEW, AUTHORIZED, CAPTURED
}
