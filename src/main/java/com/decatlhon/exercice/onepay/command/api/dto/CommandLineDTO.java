package com.decatlhon.exercice.onepay.command.api.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class CommandLineDTO {
    private int quantity;
    private String productName;
    // [Commentary only for the exercise] In the real world we probably should use another format that include the Currency type.
    private BigDecimal price;
}
