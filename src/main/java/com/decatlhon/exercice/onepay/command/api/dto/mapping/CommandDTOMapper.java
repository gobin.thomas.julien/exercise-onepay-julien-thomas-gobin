package com.decatlhon.exercice.onepay.command.api.dto.mapping;

import com.decatlhon.exercice.onepay.command.api.dto.CommandDTO;
import com.decatlhon.exercice.onepay.command.service.bean.Command;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class CommandDTOMapper {

    public static Command dtoToBean(CommandDTO dto) {
        return Command.builder()
                    .id(dto.getId())
                    .lines(ObjectUtils.isEmpty(dto.getLines()) ? new ArrayList<>() : dto.getLines().stream().map(CommandLineDTOMapper::dtoToBean).collect(Collectors.toList()))
                    .build();
    }

    public static CommandDTO beanToDTO(Command bean) {
        return CommandDTO.builder()
                .id(bean.getId())
                .lines(ObjectUtils.isEmpty(bean.getLines()) ? new ArrayList<>() : bean.getLines().stream().map(CommandLineDTOMapper::beanToDTO).collect(Collectors.toList()))
                .build();
    }

}
