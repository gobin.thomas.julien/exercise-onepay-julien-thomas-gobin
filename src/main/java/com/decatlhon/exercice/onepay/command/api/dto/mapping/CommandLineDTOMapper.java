package com.decatlhon.exercice.onepay.command.api.dto.mapping;

import com.decatlhon.exercice.onepay.command.api.dto.CommandLineDTO;
import com.decatlhon.exercice.onepay.command.service.bean.CommandLine;

public class CommandLineDTOMapper {

    public static CommandLine dtoToBean(CommandLineDTO dto) {
        return CommandLine.builder()
                .price(dto.getPrice())
                .productName(dto.getProductName())
                .quantity(dto.getQuantity())
                .build();
    }

    public static CommandLineDTO beanToDTO(CommandLine bean) {
        return CommandLineDTO.builder()
                .price(bean.getPrice())
                .productName(bean.getProductName())
                .quantity(bean.getQuantity())
                .build();
    }
}
