package com.decatlhon.exercice.onepay.command.service.bean;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class CommandLine {
    private int quantity;
    private String productName;
    private BigDecimal price;
}
