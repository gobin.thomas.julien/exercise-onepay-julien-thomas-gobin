package com.decatlhon.exercice.onepay.command.service.bean;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class Command {
    private Integer id;
    @Singular
    private List<CommandLine> lines;
}
