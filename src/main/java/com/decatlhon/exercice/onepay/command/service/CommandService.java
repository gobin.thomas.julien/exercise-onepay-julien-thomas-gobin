package com.decatlhon.exercice.onepay.command.service;

import com.decatlhon.exercice.onepay.command.service.bean.Command;
import org.springframework.stereotype.Service;

// [Commentary only for the exercise] Incomplete Service only here to show what the connection between
// TransactionService and CommandService /can/ be if both are in the same application.
@Service
public class CommandService {

    public Command postCommand(Command bean) {
        // [Commentary only for the exercise] Nothing to do here it's just for the show.
        return bean;
    }
}
