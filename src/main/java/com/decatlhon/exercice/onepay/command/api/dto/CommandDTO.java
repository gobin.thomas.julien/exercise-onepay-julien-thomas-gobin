package com.decatlhon.exercice.onepay.command.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class CommandDTO {
    private Integer id;
    @Singular
    private List<CommandLineDTO> lines;
}
