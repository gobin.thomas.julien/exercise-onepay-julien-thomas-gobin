package com.decatlhon.exercice.onepay;

import com.decatlhon.exercice.onepay.command.api.dto.CommandDTO;
import com.decatlhon.exercice.onepay.command.api.dto.CommandLineDTO;
import com.decatlhon.exercice.onepay.transaction.api.dto.TransactionDTO;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.PaymentMethod;
import com.decatlhon.exercice.onepay.transaction.service.bean.data.TransactionStatus;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OnePayApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	private CommandDTO getCommandDTO(int id) {
		return CommandDTO.builder().id(id)
				.line(CommandLineDTO.builder().price(new BigDecimal("10.00")).productName("Cool pair of ski gloves").quantity(4).build())
				.line(CommandLineDTO.builder().price(new BigDecimal("14.80")).productName("Nice wool cap").quantity(1).build())
				.build();
	}

	@Test
	public void should_PostOk() throws Exception {

		CommandDTO command = getCommandDTO(1);
		TransactionDTO dto = TransactionDTO.builder().command(command).paymentMethod(PaymentMethod.CREDIT_CARD.name()).status(TransactionStatus.NEW.name()).total(new BigDecimal("54.80")).build();

		mockMvc.perform(
				post("/transaction")
				.contentType(MediaType.APPLICATION_JSON)
				.content((new Gson()).toJson(dto))
		).andExpect(status().isOk());
	}

	@Test
	public void should_UpdateToAuthorizedOk() throws Exception {

		CommandDTO command = getCommandDTO(2);
		TransactionDTO dto = TransactionDTO.builder().command(command).paymentMethod(PaymentMethod.CREDIT_CARD.name()).status(TransactionStatus.NEW.name()).total(new BigDecimal("54.80")).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto))
		).andExpect(status().isOk());

		TransactionDTO dto2 = TransactionDTO.builder().command(command).status(TransactionStatus.AUTHORIZED.name()).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto2))
		).andExpect(status().isOk());
	}

	@Test
	public void should_UpdateToCapturedOk() throws Exception {

		CommandDTO command = getCommandDTO(3);
		TransactionDTO dto = TransactionDTO.builder().command(command).paymentMethod(PaymentMethod.CREDIT_CARD.name()).status(TransactionStatus.NEW.name()).total(new BigDecimal("54.80")).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto))
		).andExpect(status().isOk());

		TransactionDTO dto2 = TransactionDTO.builder().command(command).status(TransactionStatus.AUTHORIZED.name()).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto2))
		).andExpect(status().isOk());

		TransactionDTO dto3 = TransactionDTO.builder().command(command).status(TransactionStatus.CAPTURED.name()).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto3))
		).andExpect(status().isOk());
	}

	@Test
	public void should_NOT_UpdateToCapturedOk() throws Exception {

		CommandDTO command = getCommandDTO(4);
		TransactionDTO dto = TransactionDTO.builder().command(command).paymentMethod(PaymentMethod.CREDIT_CARD.name()).status(TransactionStatus.NEW.name()).total(new BigDecimal("54.80")).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto))
		).andExpect(status().isOk());

		TransactionDTO dto3 = TransactionDTO.builder().command(command).status(TransactionStatus.CAPTURED.name()).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto3))
		).andExpect(status().isBadRequest());
	}

	@Test
	public void should_Not_UpdateToAuthorizedOk() throws Exception {

		CommandDTO command = getCommandDTO(5);
		TransactionDTO dto2 = TransactionDTO.builder().command(command).status(TransactionStatus.AUTHORIZED.name()).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto2))
		).andExpect(status().isBadRequest());
	}

	@Test
	public void should_Not_UpdateFieldAtAuthorized() throws Exception {

		CommandDTO command = getCommandDTO(6);
		TransactionDTO dto = TransactionDTO.builder().command(command).paymentMethod(PaymentMethod.CREDIT_CARD.name()).status(TransactionStatus.NEW.name()).total(new BigDecimal("54.80")).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto))
		).andExpect(status().isOk());

		TransactionDTO dto2 = TransactionDTO.builder().command(command).paymentMethod(PaymentMethod.PAYPAL.name()).status(TransactionStatus.AUTHORIZED.name()).build();

		mockMvc.perform(
				post("/transaction")
						.contentType(MediaType.APPLICATION_JSON)
						.content((new Gson()).toJson(dto2))
		).andExpect(status().isBadRequest());
	}
}
